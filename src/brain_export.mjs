import { SlashCommandBuilder, PermissionFlagsBits, AttachmentBuilder } from 'discord.js';
import path from 'path';
import { config } from 'dotenv';
// Set up environment variables
config();

if (!process.env.BRAIN_DIR) {
  console.error(`Error: Required environment variable BRAIN_DIR is not set.`);
  process.exit(1);
}

export const data = new SlashCommandBuilder()
  .setName('brain_export')
  .setDescription('Export the bot\'s brain data.')
  .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);

export async function execute(interaction) {

  try {
    await interaction.deferReply({ ephemeral: true });
    await interaction.editReply({ content: 'Please hold on. I am working on exporting the brain data...'});
    const fileAttachment = new AttachmentBuilder(path.resolve(process.cwd(), process.env.BRAIN_DIR, 'brain.json'));
    await interaction.editReply({
      content: 'Here is the bot\'s brain data export file:',
      files: [fileAttachment],
      ephemeral: true
    });
  } catch (error) {
    console.error(error);
    await interaction.editReply({
      content: 'An error occurred while exporting the bot\'s brain data. Please check the bot logs for details.',
      ephemeral: true
    });
  }
}
